import axios from "axios";
//stokage   de l'url de notre api dans une variable
const url = 'https://api.joeleprof.com/lets-play/'
//fonction de login d,un utilisateur
export async function login(email, password) {
    try {
        //creation d'un json avec les informations du login
        const body = {
            'email': email,
            'password': password
        }
        //appel a la route login avec les parametre requis (url et notre json body)
        const { data } = await axios.post(`${url}auth/login`, body)
        if (data.success === false) {
            return false
        }
        //si l'authentification se passe bien
        sessionStorage.setItem('token', data.token)  //creation d'une variable de session token de l'utilisateur conn
        sessionStorage.setItem('isAdmin', data.isAdmin)//creation d'une variable de session isAdmin de l'utilisateur conn
        return true
    } catch (error) {
        //message d'erreur en cas de bad authentification 
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de creation d'un nouveau utilisateur
export async function create(user) {
    try {
        //appel de la route create un utilisateur avec la variable user qui contient les informations
        const { data } = await axios.post(`${url}auth/register`, user)
        if (data.success === false) {
            return false;
        }
        //stokage  du token et du isAdmin du nouveau utilisateur dans une variable de session si tout se passe bien
        sessionStorage.setItem('token', data.token)
        sessionStorage.setItem('isAdmin', data.isAdmin)
        return true
    } catch (error) {
        //message en cas d'erreur
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de trouver tout les utilisateurs par l'admin
export async function find() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token de l'utilisateur connecté
        //appel a la route qui renvoie tout les utilisateurs avec comme parametre le token
        const { data } = await axios.get(`${url}users`, { headers: { 'Authorization': `Bearer ${token}` } })
        if (data.success === false) {
            return []                  //retourne un tableau vide si on est pas autorisé
        }
        return data.data  //retourne tout les utilisateur
    } catch (error) {
        //message en cas d'erreur
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de supprimer un utilisateur par l'admin 
export async function deletebyid(id) {
    try {
        const token = sessionStorage.getItem('token') //recuperation du token de l'utilisateur connecté
        //appel a la route qui renvoie tout les utilisateurs avec comme parametre le token et le id de l'utilisateur
        const { data } = await axios.delete(`${url}users/${id}`, { headers: { 'Authorization': `Bearer ${token}` } })
        if (!data.success) {
            return false
        }
        return true
    } catch (error) {
        //message en cas d'erreur
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de modification d'un utilisateur par l'admin
export async function putuser(id, username, email) {
    try {
        //creation d'un json avec les nouvelles  informations d'un utilisateur
        const body = {
            'email': email,
            'username': username
        }
        //recuperation du token de l'admin si c'est lui qui est connecté
        const token = sessionStorage.getItem('token')
        //appel a la route qui permet de modifier un utilisateur avec comme parametre les new info et le token de l'admin
        const { data } = await axios.put(`${url}users/${id}`, body, { headers: { 'Authorization': `Bearer ${token}` } })
        if (!data.success) {
            return false
        }
        return true
    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }

}
//fonction de win score 
export async function win() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel a la route qui permet de modifier en incrementant de +1 le score du user connecté 
        const { data } = await axios.put(`${url}game/wins`, null, { headers: { 'Authorization': `Bearer ${token}` } })
        // console.log(token);
        if (!data.success) {
            return false
        }
        return true
    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de win score 
export async function restart() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel a la route qui permet de restart le score du user connecté à 0
        const { data } = await axios.put(`${url}me/reset-score`, null, { headers: { 'Authorization': `Bearer ${token}` } })
        if (!data.success) {
            return false
        }
        return true // si tout se passe bien on retourne true 

    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }

}

export async function lost() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel a la route qui permet de modifier en décremente de -1 le score du user connecté 
        const { data } = await axios.put(`${url}game/lost`, null, { headers: { 'Authorization': `Bearer ${token}` } })
        if (!data.success) {
            return false
        }
        return true // si tout se passe bien on retourne true 

    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction de recuperation des infos d'un utilisateur
export async function getMe() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel de la route qui retourne les infos du user connecté avec un token valide 
        const { data } = await axios.get(`${url}/me`, { headers: { 'Authorization': `Bearer ${token}` } })
        if (data.success === false) {
            return []
        }
        //creation d'un tableau puis insertion des infos du user dans ce tableau
        const array = []
        array.push(data.data)
        return array
    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }
}
//fonction pour qu'un utilisateur modifie ses informations
export async function putMe(username, email) {
    try {
        //creation d'un json avec les nouvelles  informations d'un utilisateur
        const body = {
            'username': username,
            'email': email
        }
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel a la route qui permet de modifier un utilisateur avec comme parametre les new info et le token du user connecté
        const { data } = await axios.put(`${url}/me`, body, { headers: { 'Authorization': `Bearer ${token}` } })

        if (!data.success) {
            return false //retourne false  si tout se passe mal
        }
        return true //retourne true si tout se passe bien
    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }
}

//fonction de supprimer un utilisateur par lui meme
export async function deleteMe() {
    try {
        const token = sessionStorage.getItem('token')//recuperation du token du user connecté
        //appel de la route qui permet a un utilisateur de se supprimer si son token est valide 
        const { data } = await axios.delete(`${url}me`, { headers: { 'Authorization': `Bearer ${token}` } })
        if (!data.success) {
            return false //retourne false  si tout se passe mal
        }
        return true  //retourne true si tout se passe bien
    } catch (error) {
        const msg = error.response.data.msg
        throw msg;
    }

}