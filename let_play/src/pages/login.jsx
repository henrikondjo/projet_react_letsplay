import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { login } from '../controllers/user';
function Login() {
    const navigate = useNavigate();
    //creation d'un usestate pour chaque attribut d'un user 
    const [email, setemail] = useState('')
    const [pswd, setpswd] = useState('')
    const [msg, setmsg] = useState('')
    //fonction qui permet de modifier la valeur du email
    const emailchange = (e) => {
        setemail(e.target.value)
    }
    //fonction qui permet de modifier la valeur du mot de passe
    const pswdchange = (e) => {
        setpswd(e.target.value)
    }
    //fonction qui appel la methode login de notre controller
    const onsubmit = async (e) => {
        try {
            e.preventDefault() //empecher la page de reload
            //validation des champ s'il ne sont pas vide avant la soumission
            if (email === "") {
                setmsg("email ivalid")
                return
            }
            if (pswd === "") {
                setmsg("mot de passe  ivalid")
                return
            }
            const response = await login(email, pswd) //appel de la methode login de notre controller
            //si tout se passe bien on vide le message d'erreur et on se dirige vers la page d'acceuil
            if (response === true) {
                setmsg('')
                navigate('/users')
            }
        } catch (error) {
            setmsg('adresse courriel ou  mot de passe Invalid !! ') //message en cas de bad authentification
        }
    }
    return (
        <div className='dow'>
            <h3>Content de vous revoir <i class='far fa-smile green-icon'></i></h3>
            <h5>Connectez-vous à votre compte</h5>
            <h6>{msg}</h6>
            <form onSubmit={onsubmit}>
                <input type='email' className='input-form' value={email} onChange={emailchange} placeholder='entrez votre email' /><br></br>
                <input type='password' onChange={pswdchange} value={pswd} className='input-form' placeholder='entrez votre mot de passe' /><br></br><br></br>
                <button className='btn btn-primary' type='submit' >Connexion</button><br></br><Link className='lien' to='/adduser'>S'inscrire ?</Link>
            </form>
        </div>
    )
}
export default Login