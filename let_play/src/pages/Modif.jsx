import React, { useState } from 'react'
import { putuser, deletebyid } from '../controllers/user';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom'
function Modif() {
    const { id } = useParams()// recuperation du id de l'utilisateur dans l'url
    const navigate = useNavigate();
    //creation d'un usestate pour chaque attribut d'un user 
    const [email, setemail] = useState('')
    const [username, setusername] = useState('')
    const [msg, setmsg] = useState('')
    //fonction qui permet de modifier la valeur du email
    const emailchange = (e) => {
        setemail(e.target.value)
    }
    //fonction qui permet de modifier la valeur du username
    const usernamechange = (e) => {
        setusername(e.target.value)
    }
    //fonction qui permet de modifier un utilisateur
    const onsubmit = async (e) => {
        try {
            e.preventDefault() //empeche a la page de reload
            //validation des champ s'il ne sont pas vide avant la soumission
            if (email === "") {
                setmsg("email ivalid")
                return
            }
            if (username === "") {
                setmsg("mot de passe  ivalid")
                return
            }
            const response = await putuser(id, username, email) //appel de la methode modifier de notre controller avec les params

            if (response === true) {
                //si tout se passe bien on se redirige vers la page d'acceuil dans 3 seconde
                setTimeout(() => {
                    setmsg('Modification en Cour....')
                    setmsg('')
                    navigate('/users')
                }, 3000);
            }
        } catch (error) {
            setmsg('adresse courriel ou  mot de passe Invalid !! ')
        }
    }
    //fonction pour supprimer un utilisateur
    const deluser = async (e) => {
        e.preventDefault()
        const response = await deletebyid(id) // supprimer un utilisateur avrc le id de params
        if (response === true) {
            navigate('/users') // //si tout se passe bien on se redirige vers la page d'acceuil 
        }
    }
    return (
        <div>
            <h1>Informations sur l'utilisateur</h1>
            <h2>{msg}</h2>
            <input type='text' className='input-form' onChange={usernamechange} placeholder='entrez votre nouveau username' /><br></br>

            <input type='email' className='input-form' onChange={emailchange} placeholder='entrez votre nouveau email' /><br></br>
            <button className='btn btn-primary' onClick={onsubmit}>Modifier</button><br></br>
            <hr></hr>
            <hr></hr>
            <h4>Supprimer cet utilisateur ? </h4><h6>cet utilisateur ne fera plus partie du jeu</h6><button className='btn btn-primary' onClick={deluser}>Supprimer</button><br></br>
            <Link className='link'
                to={`/users`}> <img src="./ret.png" alt="" width={500} height={50} /></Link>
        </div>
    )
}
export default Modif